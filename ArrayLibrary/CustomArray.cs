﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ArrayLibrary
{
    public class CustomArray<T> : IEnumerable<T>
    {
        
        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First { get; private set; }
        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last { get; private set; }
        /// <summary>
        /// Should return length of array
        /// </summary>
        public int Length { get; private set; }
        /// <summary>
        /// Should return array 
        /// </summary>
        public T[] Array { get; private set; }
        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>        
        public CustomArray(int first, int length)
        {
            throw new Exception();
        }
        /// <summary>
        /// Constructor with first index and collection
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        public CustomArray(int first, IEnumerable<T> list)
        {
            throw new Exception();
        }
        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        public CustomArray(int first, params T[] list)
        {
            throw new Exception();
        }
        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>
        /// <returns></returns>
        public T this[int item]
        {
            get
            {
                throw new Exception();
            }
            set
            {
                throw new Exception();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

    }
}
